#!/usr/bin/env python
import pandas as pd
import numpy as np
import os
from os import listdir
from os.path import isfile, join

# importing the coordinates for the gentry data
coords = pd.read_table("../Data/gentry_transect_data/gentry_coords.txt")

# getting all of the directory paths within the gentry transect data directory
area_directories = [x[0] for x in os.walk("../Data/gentry_transect_data/")][1:]

# initializing empty lists for richness and latitudes values
richness = []
lat = []

# for each of these directories
for area in area_directories:
	# get a list of all files in this directory 
	files = [f for f in listdir(area) if isfile(join(area, f))]
	# for each file in this directory 
	for filename in files:
		print("Running through: " + str(filename))
		# get the data 
		subset = pd.read_excel(area + '/' + filename, names = ['Line', 'Family', 'Genus', 'Species'], parse_cols = 3)
		# extract the family, genus, and species values and combine these 
		# to get a species name
		# take a unique list of these species names and then get the 
		# length of this...this is the richness at the given site
		richness.append(len(pd.Series.unique(subset.Family + subset.Genus + subset.Species)))
		# removing the .xls from the filename and then adding the latitude
		# to the list of latitudes
		try:
			lat.append(coords.Lat[coords.iloc[:,0] == filename[:-4].lower()].tolist()[0])
		except:
			lat.append(np.nan)

lats_richness_df = pd.DataFrame({'richness' : richness, 
								 'lat' : lat, 
								 'abs_lat' : np.abs(lat)})

lats_richness_df = lats_richness_df[lats_richness_df.lat.notnull()]
lats_richness_df.to_csv("../Data/gentry_transect_data/gentry_lats_richness.csv")
