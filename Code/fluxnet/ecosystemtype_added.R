#!/usr/bin/Rscript
#Author: Katie Hindson kah15@ic.ac.uk
#Script: ecosystemtype_added.R
#Desc: This adds the siteID ecosystem type information and the temperature
#      in Kelvin to the fluxnet_clean
#Output: fluxnet_clean_ecoK which has the appended ecosystem types and temp
#Date: Nov 2016

rm(list=ls())

load("../Data/fluxnet_clean.Rdata") # loading data into global environment

ecosystemtypes <- c() # initializing an empty vector for the ecosystem types

####################################################################################
########## Getting the indices of each site with the given ecosystem type ##########
####################################################################################

# "MF" Mixed Forests 
# getting indices for MF site IDs
# find the indices for which the specified data matches one of the entries in the
# following vector of values
eco_MF <- which(fluxnet_clean$site.id %in% c("ARSLu" , "BEBra" , "BEVie" , "CAGro" ,  "CHLae" , 
    "CNCha" , "JPSMF" , "USPFa" , "USSyv"))

# "ENF" Evergreen Needleleaf Forests
eco_ENF <- which(fluxnet_clean$site.id %in% c("ARVir" , "AUASM" , "CAMan" , "CANS1" , "CANS2" , 
        "CANS3" ,  "CANS4" ,  "CANS5" ,  "CAObs" ,  "CAQfo" , 
        "CASF1" ,  "CASF2" ,  "CATP1" ,  "CATP2" ,  "CATP3" , 
        "CATP4" ,  "CHDav" ,  "CNQia" ,  "CZBK1" ,  "DELkb" , 
        "DEObe" ,  "DETha" ,  "FIHyy" ,  "FILet" ,  "FISod" , 
        "FRLBr" ,  "ITLa2" ,  "ITLav" ,  "ITRen" ,  "ITSR2" , 
        "ITSRo" ,  "NLLoo" ,  "RUFyo" ,  "USBlo" ,  "USGBT" , 
        "USGLE" ,  "USKS1" ,  "USMe1" ,  "USMe2" ,  "USMe3" , 
        "USMe4" ,  "USMe5" ,  "USMe6" ,  "USNR1" ,  "USPrr" , 
        "USWi0" ,  "USWi2" ,  "USWi4" ,  "USWi5" ,  "USWi9"))

# "GRA" Grasslands
eco_GRA <- which(fluxnet_clean$site.id %in% c("ATNeu" ,  "AUDaP" ,  "AUEmr" ,  "AURig" ,  "AUStp" , 
          "AUYnc" ,  "CHCha" ,  "CHFru" ,  "CHOe1" ,  "CNCng" , 
          "CNDan" ,  "CNDu2" ,  "CNDu3" ,  "CNHaM" ,  "CNSw2" , 
          "CZBK2" ,  "DEGri" ,  "DERuR" ,  "DKEng" ,  "DKZaH" , 
          "ITMBo" ,  "ITTor" ,  "NLHor" ,  "PASPs" ,  "RUHa1" , 
          "RUSam" ,  "RUTks" ,  "USAR1" ,  "USAR2" ,  "USARb" , 
          "USARc" ,  "USCop" ,  "USGoo" ,  "USIB2" ,  "USLWW" , 
          "USSRG" ,  "USVar" ,   "USWkg"))

# "WSA" Woody Savannas
eco_WSA <- which(fluxnet_clean$site.id %in% c("AUAde" ,  "AUGin" ,  "AUHow" ,  "AURDF" ,  "USSRM" , 
          "USTon"))

# "SAV" Savannas
eco_SAV <- which(fluxnet_clean$site.id %in% c("AUCpr" ,  "AUDaS" ,  "AUDry" ,  "AUGWW" ,  "CGTch" , 
          "SDDem" ,  "SNDhr" ,  "ZAKru"))

# "EBF" Evergreen Broadleaf Forest
eco_EBF <- which(fluxnet_clean$site.id %in% c("AUCum" ,  "AURob" ,  "AUTum" ,  "AUWac" ,  "AUWhr" , 
          "AUWom" ,  "BRSa1" ,  "BRSa3" ,  "CNDin" ,  "FRPue" ,
          "GFGuy" ,  "GHAnk" ,  "ITCp2" ,  "ITCpz" ,  "MYPSO"))
  
# "WET" Permanent Wetlands
eco_WET <- which(fluxnet_clean$site.id %in% c("AUFog" ,  "CNHa2" ,  "CZwet" ,  "DEAkm" ,  "DESfN" , 
          "DESpw" ,  "DEZrk" ,  "DKNuF" ,  "DKZaF" ,  "FILom" , 
          "NOAdv" ,  "RUChe" ,  "SESt1" ,  "USAtq" ,  "USIvo" , 
          "USLos" ,  "USMyb" ,  "USORv" ,  "USTw1" ,  "USTw4" , 
          "USWPT"))

# "DBF" Deciduous Broadleaf Forests
eco_DBF <- which(fluxnet_clean$site.id %in% c("AULox" ,  "CAOas" ,  "CATPD" ,  "DEHai" ,  "DELnf" , 
          "DKSor" ,  "FRFon" ,  "ITCA1" ,  "ITCA3" ,  "ITCol" , 
          "ITIsp" ,  "ITPT1" ,  "ITRo1" ,  "ITRo2" ,  "JPMBF" , 
          "PASPn" ,  "USHa1" ,  "USMMS" ,  "USOho" ,  "USUMB" , 
          "USUMd" ,  "USWCr" ,  "USWi1" ,  "USWi3" ,  "USWi8" , 
          "ZMMon"))

# OSH" Open Shrublands
eco_OSH <- which(fluxnet_clean$site.id %in% c("AUTTE" ,  "CANS6" ,  "CANS7" ,  "CASF3" ,  "ESAmo" , 
          "ESLgS" ,  "ESLju" ,  "ESLn2" ,  "RUCok" ,  "USSRC" , 
          "USSta" ,  "USWhs" ,  "USWi6" ,  "USWi7"))

# "CRO" Croplands
eco_CRO <- which(fluxnet_clean$site.id %in% c("BELon" ,  "CHOe2" ,  "DEGeb" ,  "DEKli" ,  "DERuS" , 
          "DESeh" ,  "DKFou" ,  "FIJok" ,  "FRGri" ,  "ITBCi" , 
          "ITCA2" ,  "USARM" ,  "USCRT" ,  "USLin" ,  "USNe1" , 
          "USNe2" ,  "USNe3" ,  "USTw2" ,  "USTw3" ,  "USTwt"))

# "SNO" Snow and Ice
eco_SNO <- which(fluxnet_clean$site.id == "NOBlv")

# "DNF" Deciduous Needleleaf Forests
eco_DNF <- which(fluxnet_clean$site.id == "RUSkP")

# "CSH" Closed Shrublands
eco_CSH <- which(fluxnet_clean$site.id %in% c("ITNoe" ,  "RUVrk" ,  "USKS2"))


# Adding the ecosystem type names for the given indeces to the ecosystem types vector
ecosystemtypes[eco_CRO] <- "CRO"
ecosystemtypes[eco_CSH] <- "CSH"
ecosystemtypes[eco_DBF] <- "DBF"
ecosystemtypes[eco_DNF] <- "DNF"
ecosystemtypes[eco_EBF] <- "EBF"
ecosystemtypes[eco_ENF] <- "ENF"
ecosystemtypes[eco_GRA] <- "GRA"
ecosystemtypes[eco_MF]  <- "MF"
ecosystemtypes[eco_OSH] <- "OSH"
ecosystemtypes[eco_SAV] <- "SAV"
ecosystemtypes[eco_SNO] <- "SNO"
ecosystemtypes[eco_WET] <- "WET"
ecosystemtypes[eco_WSA] <- "WSA"

# renaming the data set so that I know it contains the ecosystem types and the 
# temperatures in Kelvin
fluxnet_clean_ecoK<- fluxnet_clean
# adds a new column called ecosystemtype to the fluxnet_clean data frame and 
# adds the values from 'ecosystemtypes' to this column
fluxnet_clean_ecoK$ecosystemtype <- ecosystemtypes
 
# adding a column for the temperatures in Kelvin
fluxnet_clean_ecoK$TA_F_inKelvin <- fluxnet_clean$TA_F + 273.15 

# adding the siteid to the timestampstart to make a new column of sites by day 
# and sites by month
fluxnet_clean_ecoK$siteday <-  paste0(fluxnet_clean$site.id,
                                      (substr(fluxnet_clean$TIMESTAMP_START, start = 1, stop = 8)))
fluxnet_clean_ecoK$sitemonth <- paste0(fluxnet_clean$site.id, 
                                       (substr(fluxnet_clean$TIMESTAMP_START, start = 1, stop = 6)))
fluxnet_clean_ecoK$fullmonth <- as.numeric(paste0(fluxnet_clean$year, fluxnet_clean$month))
fluxnet_clean_ecoK$fullday <- paste0(fluxnet_clean$year, sub("^", "-", fluxnet_clean$month), sub("^", "-", fluxnet_clean$day))

# adding a column for the date bus as a unitary increasing number that is the 
# difference between the earliest date of the data set and the given date 
min_day <- min(fluxnet_clean_ecoK$fullday)
fluxnet_clean_ecoK$min_day_diff <- as.integer(difftime(fluxnet_clean_ecoK$fullday, min_day, units = ("days")))


# save it to a new .Rdata file
save(fluxnet_clean_ecoK, file = "../Data/fluxnet_clean_ecoK.Rdata")
# lists the variables in the workspace...
# for some reason you need this for the dataset to save properly
ls() 