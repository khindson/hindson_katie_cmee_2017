#!usr/bin/python

""" Take in the data frmae with the column that specifies how to aggregate
the fluxnet data. Using this information, get the median temperature 
for each of the aggregated nightly data sets and only take NEE
measurements at this median temperature. Then, use fit_site_values_TPCs.py
on this resulting data. """

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import pandas as pd
import numpy as np


fluxnet = pd.read_csv("../../Data/aggregated_fluxnet_night.csv")

aggregated = pd.Series.unique(fluxnet.aggregate)

# need to make a subsetted version of the fluxnet data with only 
# the aggregate, temperature, and observation number columns...otherwsie, 
# was getting a Memory Error because the data was too big for multiple
# operations to be done on the running memory...
# So instead, make this smaller df and then use the observation number
# column at the end to extract the filtered observations from the full 
# dataset
fluxnet_micro = pd.DataFrame({'aggregate': fluxnet.aggregate, 'TA_F_inKelvin': fluxnet.TA_F_inKelvin, 'X': fluxnet.index.values})

for number in aggregated:
	print(number)
	# getting a subset of the fluxnet data at the given aggregation value
	subset = fluxnet_micro.loc[fluxnet_micro.aggregate == number]
	# getting a list of the indeces for the rows that were NOT measured at
	# the median temperature value for the given data subset
	not_med_temp_index = subset.loc[subset.TA_F_inKelvin != np.median(subset.TA_F_inKelvin)].index.values
	# dropping these values not measured at the median temperature from 
	# the full dataset
	fluxnet_micro = fluxnet_micro.drop(fluxnet_micro.index[[not_med_temp_index]])
	# resetting the index so that the indexing works in the next iteration
	fluxnet_micro = fluxnet_micro.reset_index(drop = True)

# saving this data frame with the removed non-median temperature values
# to a pickled file
fluxnet = fluxnet.loc[fluxnet_micro.X]
fluxnet.to_pickle("../../Data/fluxnet_median_temps_only.pkl")
