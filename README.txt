				    cmeeminiproject

This directory contains the Code, Data, Results and Report sub-directories for 
cmeeminiproject. My miniproject aimed to test the effects of temporal scale on 
respiration thermal performance curve-fitting using the FLUXNET data set. 

CODE DEPENDENCIES:
--> R (developed using version 3.2.3)
--> Python (developed using version 2.7.12)
--> R packages: xtable, graphics, ggplot2, plyr, reshape2, base  

Please note that I have NOT included the original FLUXNET data file 
since it is a very large file (this can be accessed
on their website, fluxnet.org). BioTraits data is not included because this 
data is in the process of being published and is not meant to be distributed.

All of the result-compiling Jupyter Notebooks should run with the data and 
results I have included in this directory. 

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

See below for the list and brief descriptions of each file contained in 
the sub-directories.

List of contents in the Code sub-directory:
==========================================

actual_data_latitude_trends.ipynb
---------------------------------

This looks at the results from fitting thermal sensitivities to the actual, 
measured FLUXNET data subset. 

Boltz_validity_test.ipynb
-------------------------

Compares the peak temperatures measured across all species in Biotraits to the 
maximum temperatures observed at all sites in FLUXNET in order to validate my
use of the Boltzmann-Arrhenius model for thermal sensitivity. 

data_cleanup_simple.R
----------------------

This is cleaning the data from the raw full data set to remove low quality 
observations, only growth period, and only sites with a number of data 
points above a certain threshold.

ecosystemtype_added.R
----------------------

This adds the siteID ecosystem type information and the temperature
in Kelvin to the clean data set (either fluxnet_clean or sample_data_clean)
and returns a new .Rdata file (either fluxnet_clean_ecoK or 
sample_data_clean_ecoK).

split_night_day.R
------------------

This parses & subsets the ecoK data into nighttime data and daytime data. It
returns two .Rdata files: one containing night data, and one containing day data
(FitDataNightHH & FitDataDayHH or sample_dataDay & sample_dataNight).

constant_species_richness_and_T_range.ipynb
-------------------------------------------

This analyzes the species richness and temperature range w.r.t. absolute latitude
simulation. 

deriving_richness_trend.ipynb
-----------------------------

Looks at the Gentry transect data and derives the function of species richness 
w.r.t. absolute latitude.

deriving_trange_trend.ipynb
---------------------------

Takes the FLUXNET median temperature aggregated data and fits a quadratic polynomial 
to the temperature range data from this. 


List of contents in the Data sub-directory:
===========================================


List of contents in the Results sub-directory:
==============================================


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


